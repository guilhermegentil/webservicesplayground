import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ServerProperties;

import java.net.InetSocketAddress;


/**
 * This class is the entry point of the application
 */
public class EmbeddedJettyWithJersey {

    private static final String HOST = "localhost";
    private static final int PORT = 9999;
    private static final String API_ROOT_PATH = "/api/*";
    private static final String API_PACKAGE = "com.zsoft.api";

    public static void main(String[] args) throws Exception {
        EmbeddedJettyWithJersey app = new EmbeddedJettyWithJersey();

        app.start();
    }

    private void start() throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        Server server = new Server(InetSocketAddress.createUnresolved(HOST, PORT));

        ServletHolder servlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, API_ROOT_PATH);

        // Tells the Jersey Servlet which REST service/class to load.
        servlet.setInitParameter(ServerProperties.PROVIDER_PACKAGES, API_PACKAGE);

        // Enable request log
        HandlerCollection handlers = new HandlerCollection();
        ContextHandlerCollection contexts = new ContextHandlerCollection();

        server.setHandler(handlers);

        handlers.addHandler(context);

        NCSARequestLog requestLog = new NCSARequestLog();
        requestLog.setFilename("yyyy_mm_dd.request.log");
        requestLog.setFilenameDateFormat("yyyy_MM_dd");
        requestLog.setRetainDays(90);
        requestLog.setAppend(true);
        requestLog.setExtended(true);
        requestLog.setLogCookies(false);
        requestLog.setLogTimeZone("GMT");
        RequestLogHandler requestLogHandler = new RequestLogHandler();
        requestLogHandler.setRequestLog(requestLog);
        handlers.addHandler(requestLogHandler);

        server.start();
        server.join();
        server.destroy();
    }

}
